package com.liuqiang.demo.controller;

import com.liuqiang.demo.properties.GirlProperties;
import com.liuqiang.demo.respository.UserRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.web.bind.annotation.*;


/**
 * 测试代码
 */
@RestController
public class HelloController {

    /*
    @Value("${cupSize}")
    private String size;     // 反向注入信息 @Value(${"配置文件里面的字段"})

    @Value("${age}")
    private int age;

    @Value("${content}")
    private String info;

    */

    @Autowired
    private GirlProperties girlProperties;

    @Autowired
    private UserRespository userRespository;

    @RequestMapping(value = {"/hello" , "/hi"} , method = RequestMethod.GET)
    public String say(@RequestParam( value = "id" , required = false , defaultValue = "1000") int myId){
//        return "ID : " + myId;
        return girlProperties.getCupSize() + "  liuqiang   " + girlProperties.getAge();
    }

    @RequestMapping(value = "/login" , method = RequestMethod.POST)
    public void login(@RequestBody JSONObject jsonParam){
        System.out.println(jsonParam);
    }
}
