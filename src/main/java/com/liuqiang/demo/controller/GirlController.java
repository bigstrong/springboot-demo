package com.liuqiang.demo.controller;

import com.liuqiang.demo.aspect.HttpAspect;
import com.liuqiang.demo.domain.Girl;
import com.liuqiang.demo.domain.InvitCode;
import com.liuqiang.demo.domain.Rsult;
import com.liuqiang.demo.respository.GirlRespository;
import com.liuqiang.demo.respository.InviteRespository;
import com.liuqiang.demo.service.GirlService;
import com.liuqiang.demo.service.InviteCodeService;
import com.liuqiang.demo.utils.RsultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class GirlController {

    private final static Logger logger = LoggerFactory.getLogger(GirlController.class);

    @Autowired
    private GirlRespository girlRespository;

    @Autowired
    private GirlService girlService;

    @Autowired
    private InviteCodeService inviteCodeService;


    /**
     * 返回女生列表
     */
    @GetMapping(value = "/girls")
    public List<Girl> girlList(){
       // System.out.println("girlList");   //验证 @Before标签的先后

        logger.info("处理中:");

        return girlRespository.findAll();
    }

    /**
     * 添加一个女生
     * @param
     * @param
     * @return
     */
    @PostMapping(value = "/girls")
    public Rsult<Girl> girlAdd(@Valid Girl girl , BindingResult bindingResult){
          if(bindingResult.hasErrors())
              return RsultUtil.err(1, bindingResult.getFieldError().getDefaultMessage());
        return RsultUtil.success(girlRespository.save(girl));
    }

    /**
     * 通过ID来查询一个女生
     * @param id
     * @return
     */
    @GetMapping(value = "/girls/{id}")
    public Girl girlSelect(@PathVariable("id") Integer id){

        Optional<Girl> girl = girlRespository.findById(id);
        return girl.get();
    }

    /**
     * 通过ID来修改一个女生信息
     * @param id
     * @param cupSize
     * @param age
     */
    @PutMapping(value = "/girls/{id}")
    public Girl girlUpdate(@PathVariable("id") Integer id ,
                           @RequestParam("cupSize") String cupSize ,
                           @RequestParam("age") Integer age
                           ){
        Girl girl = new Girl();
        girl.setId(id);
        girl.setCupSize(cupSize);
        girl.setAge(age);

        return girlRespository.save(girl);
    }

    /**
     * 通过ID来删除一个女生
     * @param id
     */
    @DeleteMapping(value = "/girls/{id}")
    public void girlDelect(@PathVariable("id") Integer id){
        girlRespository.deleteById(id);
    }

    @PostMapping(value = "/girls/addTwo")
    public void girlAddTwo(){
        girlService.insertTwo();
    }

    @GetMapping(value = "/girls/getAge/{id}")
    public void getAge(@PathVariable("id") Integer id) throws Exception {
        girlService.getAge(id);
    }

    @GetMapping(value = "/inviterText")
    public InvitCode InviterText(){
        InvitCode invitCode = inviteCodeService.Text();
        return invitCode;
    }

}
