package com.liuqiang.demo.utils;

import com.liuqiang.demo.domain.Rsult;

public class RsultUtil {
    public static Rsult success(Object object){
        Rsult rsult = new Rsult();
        rsult.setData(0);
        rsult.setMsg("成功");
        rsult.setData(object);
        return rsult;
    }

    public static Rsult success(){
        return success(null);
    }

    public static Rsult err(Integer code , String msg){
        Rsult rsult = new Rsult();
        rsult.setCode(code);
        rsult.setMsg(msg);
        rsult.setData(null);
        return rsult;
    }
}
