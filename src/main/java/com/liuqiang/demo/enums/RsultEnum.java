package com.liuqiang.demo.enums;

public enum RsultEnum {
    UNKONW_ERROW(-1 , "未知错误"),
    SUCCESS(0 , "成功"),
    PRI_SCHOOL(100 , "还在上小学"),
    HIGH_SCHOOL(101 , "还在上初中"),
    NULL_INFO(110 , "空指针")
    ;

    private Integer code;

    private String msg;

    RsultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
