package com.liuqiang.demo.service;

import com.liuqiang.demo.domain.Girl;
import com.liuqiang.demo.enums.RsultEnum;
import com.liuqiang.demo.exception.GirlException;
import com.liuqiang.demo.respository.GirlRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class GirlService {

    @Autowired
    GirlRespository girlRespository;

    @Transactional  //事物注解
    public void insertTwo(){
        Girl girlA = new Girl();
        girlA.setAge(60);
        girlA.setCupSize("G");
        girlRespository.save(girlA);

        Girl girlB = new Girl();
        girlB.setAge(70);
        girlB.setCupSize("FFFFFFFFF");
        girlRespository.save(girlB);
    }

    public void getAge(Integer id ) throws Exception {

        Optional<Girl> girl = girlRespository.findById(id);
        Integer age = girl.get().getAge();
        if(age < 20){
            //返回"你还在上小学吧" code = 100
            throw new GirlException(RsultEnum.PRI_SCHOOL);
        }else if(age > 20 && age < 40){
            //返回"你还在上初中" code = 101
            throw new GirlException(RsultEnum.HIGH_SCHOOL);
        }
    }

    /**
     * 通过ID查询一个女生的信息
     * @param id
     * @return
     */
    public Girl findone(Integer id ){
        Optional<Girl> girl = girlRespository.findById(id);
        return girl.get();
    }
}
