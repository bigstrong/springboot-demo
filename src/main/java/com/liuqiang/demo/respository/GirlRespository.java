package com.liuqiang.demo.respository;

import com.liuqiang.demo.domain.Girl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GirlRespository extends JpaRepository<Girl, Integer> {
}
