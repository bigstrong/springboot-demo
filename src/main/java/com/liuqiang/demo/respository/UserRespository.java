package com.liuqiang.demo.respository;

import com.liuqiang.demo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRespository extends JpaRepository<User, Integer> {
}
