package com.liuqiang.demo.respository;

import com.liuqiang.demo.domain.InvitCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InviteRespository extends JpaRepository<InvitCode , Integer> {
}
