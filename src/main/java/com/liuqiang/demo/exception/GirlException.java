package com.liuqiang.demo.exception;

import com.liuqiang.demo.enums.RsultEnum;

/**
 * Springboot 只针对 RuntimeException 的事物才会进行回滚
 */
public class GirlException extends RuntimeException {

    private Integer code;

    public GirlException(RsultEnum rsultEnum) {
        super(rsultEnum.getMsg());
        this.code = rsultEnum.getCode();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "GirlException{" +
                "code=" + code +
                '}';
    }
}
