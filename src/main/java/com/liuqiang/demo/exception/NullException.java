package com.liuqiang.demo.exception;

import com.liuqiang.demo.enums.RsultEnum;

public class NullException extends RuntimeException {

    private Integer code;

    public Integer getCode() {
        return code;
    }

    public NullException(RsultEnum rsultEnum) {
        super(rsultEnum.getMsg());
        this.code = rsultEnum.getCode();
    }

    @Override
    public String toString() {
        return "NullException{" +
                "code=" + code +
                '}';
    }
}
