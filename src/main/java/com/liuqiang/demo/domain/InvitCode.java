package com.liuqiang.demo.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class InvitCode {
    @Id
    @GeneratedValue
    private Integer id;

    private String inviteCo;

    private long userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInviteCo() {
        return inviteCo;
    }

    public void setInviteCo(String inviteCo) {
        this.inviteCo = inviteCo;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "InvitCode{" +
                "id=" + id +
                ", inviteCo='" + inviteCo + '\'' +
                ", userId=" + userId +
                '}';
    }
}
