package com.liuqiang.demo.aspect;

import com.liuqiang.demo.enums.RsultEnum;
import com.liuqiang.demo.exception.NullException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class HttpAspect {

    //导包的时候 导org.slf4j.*
    private final static Logger logger = LoggerFactory.getLogger(HttpAspect.class);

    @Before("execution(public * com.liuqiang.demo.controller.GirlController.*(..))")
    public void log(JoinPoint joinPoint){
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        //url
        logger.info("url={}" , request.getRequestURL());

        //method
        logger.info("method={}" , request.getMethod());

        //ip
        logger.info("ip={}" , request.getRemoteAddr());

        //类方法
        logger.info("class_method={}", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());

        //参数
        logger.info("args={}" , joinPoint.getArgs());
    }

    @After("execution(public * com.liuqiang.demo.controller.GirlController.*(..))")
    public void afterDo(){
        logger.info("处理后");
    }

    @AfterReturning(returning = "object", value = "execution(public * com.liuqiang.demo.controller.GirlController.*(..))")
    public void afterReturn(Object object){
        if (object == null)
            throw new NullException(RsultEnum.NULL_INFO);
        logger.info("response={}" , object.toString());

    }
}
