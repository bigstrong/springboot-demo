package com.liuqiang.demo.handle;

import com.liuqiang.demo.controller.GirlController;
import com.liuqiang.demo.domain.Rsult;
import com.liuqiang.demo.exception.GirlException;
import com.liuqiang.demo.exception.NullException;
import com.liuqiang.demo.utils.RsultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionHandle {

    private final static Logger logger = LoggerFactory.getLogger(ExceptionHandle.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Rsult handle(Exception e){
        if(e instanceof GirlException){
            GirlException girlException = (GirlException)e;
            return RsultUtil.err(girlException.getCode() , girlException.getMessage());
        }else if (e instanceof NullException){
            NullException nullException = (NullException)e;
            return RsultUtil.err(nullException.getCode() , nullException.getMessage());
        } else {
            logger.info("系统异常 {}" , e );
            return RsultUtil.err(-1, "未知错误");

        }
    }
}
