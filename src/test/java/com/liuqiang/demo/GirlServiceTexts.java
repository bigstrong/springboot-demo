package com.liuqiang.demo;

import com.liuqiang.demo.domain.Girl;
import com.liuqiang.demo.service.GirlService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)    //我们要在测试环境里面跑了
@SpringBootTest
public class GirlServiceTexts {

    @Autowired
    private GirlService girlService;
    @Test
    public void findOneTest(){
        Girl girl = girlService.findone(26);
        Assert.assertEquals(new Integer(19),girl.getAge());
    }
}
